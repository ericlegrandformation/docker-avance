# DOCKER - Administration avancée

  

![a](https://www.docker.com/sites/default/files/d8/2019-07/Docker%20Enterprise%20graphic.jpg?zoomresize=480%2C240)

Ce projet va vous permettre de :

* De provisionner et configurer 3 VMs "Ubuntu 16.04 Server" dans VirtualBox avec Vagrant et Ansible
* Pré-configurer ces 3 VMs automatiquement
* Installer manuellement Docker EE, Swarm, un DTR et un UCP 



## Pour commencer

Ce projet a été réalisé avec un ordinateur sous UBUNTU 16.04 LTS avec 16Go de RAM disposant d'au moins 60 Go d'espace disque afin d'être à l'aise lors de la création et l'utilisation des VM.

  

### Pré-requis

Ce qui est requis pour commencer avec ce projet...

- un ordinateur connecté à internet préférablement avec Linux Ubuntu 16.04 LTS
- des connaissances de base en système Linux, réseaux informatique et applications Web
- des notions de programmation (scripting, Python, PHP, ..)
- les logiciels utilisés sont:
	- git version 2.7.4
	- virtualbox 5.2.32
	- Vagrant 2.0.4
	- ansible 2.8.4
	- python 3.5.2

  

### Pour l'installation de Docker EE, UCP et DTR

Vous aller devoir aussi:
- créer ou disposer d'un compte sur [https://hub.docker.com/](https://hub.docker.com/)
- vous logger sur hub.docker.com puis dans le menu "explore", sous-menu "Docker EE", sélectionner les filtres "server", "Linux", et "x86-64"
- cliquer sur "Docker Enterprise (Ubuntu)" puis sur "Start 1 Month Trial"
- Copier l'URL à partir du champ "Copy and paste this URL" pour télécharger l'adresse URL de votre version d'évaluation DOCKER-EE. (Vous allez utiliser plutard cette URL).
- Télécharger le fichier licence "docker_subscription.lic" pour pouvoir l'utiliser plutard
  

## Démarrage

Sur votre machine hôte, depuis votre répertoire **/home/'votre nom utilisateur'**, dans un terminal, taper successivement :
```
# depuis /home/'votre nom utilisateur'
$ git clone https://framagit.org/ericlegrandformation/docker-avance.git
```
* ouvrer le dossier "docker-avance" **avec VSCode**

* ouvrer un **nouveau terminal** dans VSCode

* créer un environnement virtuel python avec la commande :
```
$ virtualenv --python=python3.5 venv
```
* activer l'environnement virtuel python avec la commande :
```
$ source ./venv/bin/activate
```

* installer quelques pré-requis avec la commande :
```
$ sudo apt install libffi-dev python-dev libssl-dev
```

* installer ansible avec la commande :
```
$ pip install ansible
```

* vérifier la version ansible avec :
```
$ ansible --version
(le retour = version 2.8 mini)
```
* Lancer la création des VM VirtualBox avec la commande :
```
$ vagrant up
```
* Configurer la VM Ansible avec la commande:
```
$ ansible-playbook setup_all.yml
```

* Metter à jour les fichiers /etc/hosts des 3 machines avec:
```
$ ansible-playbook updatehosts.yml
```

* Configurer l'accès sécuriser ssh des 3 machines avec :
```
$ ansible-playbook sshkeys-exchange
```
### Pour vérifier le bon fonctionnement des 3 VM crées
*  **Après avoir redémarrer les 3 machines** depuis VirtualBox

* Vérifier que vous pouvez vous connecter aux 3 machines avec:
	- $ **vagrant ssh swarm1**
	- $ **vagrant ssh dockernode1**
	- $ **vagrant ssh dockernode2**

## Installer Docker EE sur les 3 VM

* installer Docker EE sur les 3 VM en suivant le mode d'emploi: [https://docs.docker.com/install/linux/docker-ee/ubuntu/](https://docs.docker.com/install/linux/docker-ee/ubuntu/) 
	- en commençant par vous connecter sur swarm1 avec "vagrant ssh swarm1", 
	- puis répéter les mêmes opérations après vous être connecté sur dockernode1 avec "vagrant ssh dockernode1" 
	- et enfin sur dockernode2 avec "vagrant ssh dockernode2"
	
```
$ DOCKER_EE_URL="<DOCKER-EE-URL>"
```
"DOCKER-EE-URL" est l'URL récupérée au paragraphe "Pour l'installation de Docker EE, UCP et DTR".

```
$ DOCKER_EE_VERSION=18.09
```
"DOCKER_EE_VERSION est la version à installer.
```
$ curl -fsSL "${DOCKER_EE_URL}/ubuntu/gpg" | sudo apt-key add -
```
L'empreinte:
```
$ sudo apt-key fingerprint 6D085F96

pub   4096R/0EBFCD88 2017-02-22
      Key fingerprint = DD91 1E99 5A64 A202 E859  07D6 BC14 F10B 6D08 5F96
uid                  Docker Release (EE deb) <docker@docker.com>
sub   4096R/6D085F96 2017-02-22
```
Le repository:
```
sudo add-apt-repository \
   "deb [arch=$(dpkg --print-architecture)] $DOCKER_EE_URL/ubuntu \
   $(lsb_release -cs) \
   stable-$DOCKER_EE_VERSION"
```
Update:
```
$ sudo apt-get update
```
L'installation des package Docker EE:
```
sudo apt-get install docker-ee docker-ee-cli containerd.io
```
* Pour pouvoir executer docker sans sudo, 
```
# sudo usermod -aG docker $USER
# sudo usermod -aG docker trainee
```

## Configuration de SWARM

* Initialiser SWARM sur la VM swarm1:
```
vagrant@swarm1:~_# docker swarm init --advertise-addr 192.168.2.9  
vagrant@swarm1:~_# docker swarm join-token worker   
To add a worker to this swarm, run the following command:
 
docker swarm join --token <tocken_pour_worker> 192.168.2.9:2377

vagrant@swarm-1:~_# docker swarm join-token manager  
To add a manager to this swarm, run the following command: 

docker swarm join --token <token_pou_manager> 192.168.2.9:2377

```
### Ajouter les VM dockernode1 et dockernode2 comme worker au SWARM
```
# depuis un terminal
vagrant@dockernode1:~_# vagrant ssh dockernode1
vagrant@dockernode1:~_# docker swarm join --token <tocken_pour_worker> 192.168.2.9:2377
vagrant@dockernode1:~_# exit

vagrant@dockernode2:~_# vagrant ssh dockernode2
vagrant@dockernode2:~_# docker swarm join --token <tocken_pour_worker> 192.168.2.9:2377
vagrant@dockernode2:~_# exit

```

## Installer UPC sur swarm1:
```
vagrant@swarm-1:~_# docker container run --rm -it --name ucp \
  -v /var/run/docker.sock:/var/run/docker.sock \
  docker/ucp:2.2.4 install \
  --host-address 192.168.2.9 \
  --interactive
  ...
  ...
  # répondez aux questions
  Admin Username: admin
  Admin Password: fenestros
  Confirm Admin Password: fenestros
  ...
  Please enter your Docker Hub username: <votre username docker hub>
  Please enter your Docker Hub password: <votre passwd docker hub>
  ...
  Additional aliases: <taper entrée>
  ...
  # A la fin de l'installation
  INFO[0065] Login to UCP at https://192.168.2.9:443      
  INFO[0065] Username: admin                              
  INFO[0065] Password: (your admin password)
   
```
* Lancer FireFox pour tester l'installation à l'adresse https://192.168.2.9:443, pour accepter le risque, cliquer sur "Avancé..." puis sur "Accepter le risque et poursuivre"

* Saisir votre login et mot de passe, vous êtes accueilli par " Your system is unlicensed."

* Cliquer sur "upload license" puis sélectionner votre fichier "docker_subscription.lic" et valider.

*Bravo, vous pouvez maintenant voir le Dashboard UPC*

## Installation du DTR sur dockernode1

```
vagrant@dockernode1:~_# docker run -it --rm \
  docker/dtr install \
  --ucp-node dockernode1 \
  --ucp-username admin \
  --ucp-url https://192.168.2.9 \
  --ucp-insecure-tls
  ...
  # Répondre aux questions posées
  ucp-password: fenestros
  ...
   # A la fin de l'installation
   INFO[0269] Successfully registered dtr with UCP         
   INFO[0270] Installation is complete                     
   INFO[0270] Replica ID is set to: f99749db6487           
   INFO[0270] You can use flag '--existing-replica-id f99749db6487' when joining other replicas to your Docker Trusted Registry Cluster
  
```
* Lancer FireFox pour tester l'installation à l'adresse https://192.168.2.10:443, pour accepter le risque, cliquer sur "Avancé..." puis sur "Accepter le risque et poursuivre"

* Saisir votre login et mot de passe

*Bravo, vous pouvez maintenant voir le Dashboard DTR*

## C'est fini.

*Bravo, vous venez d'installer manuellement Docker EE, Swarm, DTR et UPC sur 3 machines virtuelles UBUNTU server 16.04 LTS.*


***
## Versions
  

**Dernière version stable :** 0.1

**Dernière version :** 0.1

Liste des versions : [Cliquer pour afficher](https://framagit.org/ericlegrandformation/ansible.git)

  

## Auteurs

  

*  **Eric Legrand**  _alias_  [@ericlegrand](https://framagit.org/ericlegrandformation/ansible.git)

avec le soutien de Messieurs Hugh Norris et Elie Gavoty.
```
